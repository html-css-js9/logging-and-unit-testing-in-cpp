#include "pch.h"
#include "MathLibrary.h"
#include "gtest/gtest.h"

using namespace MathLibrary;

TEST(TestMath, TestPos) {
  EXPECT_EQ(11, Arithmetic::Divide(121.0, 11.0));
  EXPECT_EQ(132, Arithmetic::Add(121.0, 11.0));
  EXPECT_EQ(1331, Arithmetic::Multiply(121.0, 11.0));
  EXPECT_THROW(Arithmetic::Divide(121.0, 0.0), std::invalid_argument);
  EXPECT_TRUE(true);
}