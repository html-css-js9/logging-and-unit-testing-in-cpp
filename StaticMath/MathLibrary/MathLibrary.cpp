#include "MathLibrary.h"
#include <stdexcept>

namespace MathLibrary {
    double Arithmetic::Add(double a, double b) {
        return a + b;
    }

    double Arithmetic::Subtract(double a, double b) {
        return a - b;
    }

    double Arithmetic::Multiply(double a, double b) {
        return a * b;
    }

    double Arithmetic::Divide(double a, double b) {
        if (b == 0) {
            throw std::invalid_argument("You cannot divide by zero!");
        }
        return a / b;
    }
}