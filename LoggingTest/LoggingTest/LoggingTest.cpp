#define _ITERATOR_DEBUG_LEVEL 0

#include <iostream>
#include "easylogging++.h"
#include "TestFile.cpp"

INITIALIZE_EASYLOGGINGPP

int main(int argc, const char** argv) {
    // Load configuration from file
    el::Configurations conf("C:/Users/Seo Legna/Documents/FortyTwoLabs/Tutorials/LoggingAndUnitTesting/LoggingTest/easyLoggingConfAndOut/my-conf.conf");
    // Reconfigure single logger
    el::Loggers::reconfigureLogger("default", conf);
    // Actually reconfigure all loggers instead
    el::Loggers::reconfigureAllLoggers(conf);
    // Now all the loggers will use configuration from file
    Test test(2.2, 3.3);
    test.Add();
    return 1;
}