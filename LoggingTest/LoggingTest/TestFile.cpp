#include <iostream>
#include "easylogging++.h"

using namespace std;

class Test {
private:

	double a;
	double b;

public:

	Test(double aValue, double bValue) {
		LOG(DEBUG) << "Test Object created with values: " << aValue << " and " << bValue << endl;
		this->a = aValue;
		this->b = bValue;
	}

	Test() {
		LOG(DEBUG) << "Test Object created without values."<< endl;
	}

	void setA(double value) {
		this->a = value;
	}

	void setB(double value) {
		this->b = value;
	}

	double getA() {
		return this->a;
	}

	double getB() {
		return this->b;
	}

	double Add(double a, double b) {
		LOG(INFO) << "Add function called, with parameters: " << a << " and " << b << endl;
		return a + b;
	}

	double Add() {
		LOG(DEBUG) << "Add function called, without prameters." << endl;
		return this->a + this->b;
	}

	double Multiply(double a, double b) {
		LOG(INFO) << "Multiply function called, with parameters: " << a << " and " << b << endl;
		return a * b;
	}

	double Multiply() {
		LOG(DEBUG) << "Multiply function called, without prameters."<< endl;
		return this->a * this->b;
	}
};